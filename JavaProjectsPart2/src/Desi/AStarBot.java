package Desi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import snakes.Bot;
import snakes.Coordinate;
import snakes.Direction;
import snakes.Snake;

public class AStarBot implements Bot {
    // Map to store the path from each coordinate to the target
    private Map<Coordinate, Coordinate> cameFrom;
    // Map to store the cost of each coordinate
    private Map<Coordinate, Integer> costSoFar;
    // List of coordinates to visit
    private List<Coordinate> openList;
    // List of visited coordinates
    private List<Coordinate> closedList;
    // The target coordinate
    private Coordinate target;

    @Override
    public Direction chooseDirection(Snake snake, Snake opponent, Coordinate mazeSize, Coordinate apple) {
        cameFrom = new HashMap<>();
        costSoFar = new HashMap<>();
        openList = new LinkedList<>();
        closedList = new ArrayList<>();
        target = apple;

        // Initialize the starting coordinate
        Coordinate head = snake.getHead();
        openList.add(head);
        costSoFar.put(head, 0);

        // A* algorithm loop
        while (!openList.isEmpty()) {
            // Get the coordinate with the lowest cost
            Coordinate current = getLowestCostCoordinate();

            // Check if we have reached the target
            if (current.equals(target)) {
                break;
            }

            openList.remove(current);
            closedList.add(current);



            // Check the neighboring coordinates
            for (Coordinate neighbor : current.getNeighbors(current)) {
                if (neighbor.inBounds(mazeSize) && !snake.occupies(neighbor) && !opponent.occupies(neighbor)) {
// Calculate the cost to reach the neighbor
                    int newCost = costSoFar.get(current) + 1;
                    // Check if we have a better path to this neighbor
                    if (!costSoFar.containsKey(neighbor) || newCost < costSoFar.get(neighbor)) {
                        costSoFar.put(neighbor, newCost);
                        int priority = newCost + calculateManhattanDistance(neighbor, target);

                        // Add the neighbor to the open list
                        if (!openList.contains(neighbor)) {
                            openList.add(neighbor);
                        } else {
                            openList.remove(neighbor);
                            openList.add(neighbor);
                        }

                        // Update the cameFrom map
                        cameFrom.put(neighbor, current);
                    }
                }
            }
        }

        // Retrace the path from the target to the starting point
        Coordinate current = target;
        List<Coordinate> path = new ArrayList<>();
        while (!current.equals(head)) {
            path.add(current);
            current = cameFrom.get(current);
        }

        // Get the direction of the next step
        Coordinate nextStep = path.get(path.size() - 2);
        return head.directionTo(nextStep);
    }

    // Method to get the coordinate with the lowest cost
    private Coordinate getLowestCostCoordinate() {
        return Collections.min(openList, Comparator.comparing(coord -> costSoFar.get(coord)));
    }

    // Method to calculate the Manhattan distance
    private int calculateManhattanDistance(Coordinate a, Coordinate b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
    }
}

//


//    This code
//    uses the A*algorithm to find the shortest path from the snake's current position to the apple.
//        It uses a priority queue(openList)to store the coordinates that still need to be visited,and a
//        list(closedList)to store the coordinates that have already been visited.The cameFrom map stores the
//        path from each coordinate to the target,and the cost
