package Desi;

import snakes.Bot;
import snakes.Coordinate;
import snakes.Direction;
import snakes.Snake;

import java.util.List;

public class SnakeBot implements Bot {
    // Constants for the direction of the snake
    private static final int LEFT = 0;
    private static final int UP = 1;
    private static final int RIGHT = 2;
    private static final int DOWN = 3;

    // The current position of the snake's head
    private final int x;
    private final int y;

    // The current direction of the snake
    private final int currentDirection;

    // The board on which the snake is playing
    private final int[][] board;

    public SnakeBot(int x, int y, int currentDirection, int[][] board, List<int[]> body) {
        this.x = x;
        this.y = y;
        this.currentDirection = currentDirection;
        this.board = board;
        // The list of coordinates for the snake's body
    }

    // Method to move the snake towards the food
    public void moveTowardsFood(int[] food) {
        // Calculate the distance to the food in the x and y directions
        int distanceX = food[0] - x;
        int distanceY = food[1] - y;

        // Prioritize the direction that brings the snake closer to the food
        if (Math.abs(distanceX) > Math.abs(distanceY)) {
            if (distanceX > 0) {
                // Move right
                move(RIGHT);
            } else {
                // Move left
                move(LEFT);
            }
        } else {
            if (distanceY > 0) {
                // Move down
                move(DOWN);
            } else {
                // Move up
                move(UP);
            }
        }
    }

    // Method to move the snake in a particular direction
    private void move(int direction) {
        // Check if the new direction is a sharp turn
        boolean sharpTurn = Math.abs(currentDirection - direction) == 2;

        // Check if the new direction will cause a collision
        boolean collision = false;
        int newX = x;
        int newY = y;
        if (direction == LEFT) {
            newX--;
        } else if (direction == UP) {
            newY--;
        } else if (direction == RIGHT) {
            newX++;
        } else if (direction == DOWN) {
            newY++;
        }
        if (newX < 0 || newX >= board[0].length || newY < 0 || newY >= board.length || board[newY][newX] == 1) {
            collision = true;
        }
    }

    @Override
    public Direction chooseDirection(Snake snake, Snake opponent, Coordinate mazeSize, Coordinate apple) {
        return null;
    }
}
