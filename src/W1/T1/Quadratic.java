package W1.T1;

import java.util.Scanner;

public class Quadratic {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double b = Double.parseDouble(scanner.nextLine());
        double c = Double.parseDouble(scanner.nextLine());

        double discriminant = b * b - 4.0 * c;
        double sqroot = Math.sqrt(discriminant);

        if (discriminant > 0) {
            //two real roots
            double root1 = (-b + sqroot) / 2.0;
            double root2 = (-b - sqroot) / 2.0;
            System.out.println(root1);
            System.out.println(root2);
        }else if (discriminant==0){
            double root1= -b/2.0;
            System.out.println(root1);
        }else{
            System.out.println("No real number");
        }
    }
}
