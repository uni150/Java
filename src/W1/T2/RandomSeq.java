package W1.T2;

import java.util.Scanner;

public class RandomSeq {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // command-line argument
        int n = Integer.parseInt(scanner.nextLine());

        // generate and print n numbers between 0 and 1
        for (int i = 0; i < n; i++) {
            System.out.println(Math.random());
        }
    }
}
