package W1.T3;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: RandomSeqAB
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/30/2022
 *
 * Method : Ad-Hoc
 */

import java.util.Random;
import java.util.Scanner;

public class RandomSeqAB_v2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //input : conditions
        int range1 = scanner.nextInt();
        int range2 = scanner.nextInt();
        int numb = scanner.nextInt();

        int randomIntegerRange = 0;
        for (int i = 0; i < numb; i++) {
            randomIntegerRange = getRandomIntegerWithinRange(range1, range2);
            System.out.print(randomIntegerRange + " ");
        }

    }
    private static int getRandomIntegerWithinRange(int min, int max) {
        int spread = max - min;
        return new Random().nextInt(spread + 1) + min;
    }
}
