package W1.T6;

/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: Hello World
 * Link: https://open.kattis.com/problems/hello
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/30/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:0.06 s
 */

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}

