package W1.T6;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: Just a Minute
 * Link: https://open.kattis.com/problems/justaminute
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/30/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:	0.15 s
 */
import java.util.Scanner;

public class JustAMinute {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int cases = scan.nextInt();
        int mins = 0;
        int length = 0;

        for (int i = 0; i < cases; i++) {
            mins += scan.nextInt();
            length += scan.nextInt();
        }

        if (length <= mins * 60)
            System.out.println("measurement error");
        else
            System.out.println((double) length / (mins * 60));

        scan.close();
    }

}

