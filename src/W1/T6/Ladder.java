package W1.T6;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: Ladder
 * Link:https://open.kattis.com/problems/ladder
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/30/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.10 s
 */

import java.util.Scanner;

public class Ladder {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //input for height and angle degrees
        int height = scan.nextInt();
        int angleDegree = scan.nextInt();

        //calculate the minimum possible length of the ladder
        System.out.println((int) (Math.ceil(1 / (Math.sin(angleDegree * (Math.PI / 180)) / height))));

        scan.close();
    }

}

