package W1.T6;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: Bijele
 * Link: https://open.kattis.com/problems/bijele
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/30/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:0.12 s
 */

import java.util.Scanner;

public class bijele {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // store the set pieces
        int[] numberPieces = {1, 1, 2, 2, 2, 8};

        // array for the pieces we have
        int[] numbersAvaible = new int[6];

        //input the numbers of the pieces we have
        for (int i = 0; i < numbersAvaible.length; i++)
            numbersAvaible[i] = scan.nextInt();

        //calculate the differences and print the final result
        for (int i = 0; i < numbersAvaible.length; i++)
            System.out.print(numberPieces[i] - numbersAvaible[i] + " ");
    }
}

