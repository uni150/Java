package W1.T6;
/*
* Advanced Object Oriented Programming with Java, WS 2022
* Problem: Faktor
* Link: https://open.kattis.com/problems/faktor
*
* @Desislava Radusheva
* @Gergana Georgieva
* @version 1.0, 11/19/2022
*
* Method :Ad-Hoc
* Status : Accepted
* Runtime: 0.10 s
*/

import java.util.Scanner;

public class faktor {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        //input for the articles number and the impact factor
        double articlesNumber = scan.nextInt();
        double impactFactor = scan.nextInt();

        impactFactor--;

        System.out.println((int) ((articlesNumber * impactFactor) + 1)); //calculate and printthe  minimal number of scientists the man need to bribe.

        scan.close();

    }
}


