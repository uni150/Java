package W1.T6;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: Stuck In A Time Loop
 * Link:https://open.kattis.com/problems/timeloop
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 11/04/2022
 * <p>
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.13 s
 */
import java.util.Scanner;
public class stuckTimeLoop{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = scan.nextInt();

        for (int i = 1; i <= number; i++)
            System.out.println(i + " Abracadabra");

        scan.close();
    }
}
