package W2.T4;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem:
 * Link:
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/30/2022
 *
 * Method :
 * Status : Accepted
 * Runtime:0.12 s
 */


import java.util.Scanner;

public class PaulEugen {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int servesNumber = Integer.parseInt(scanner.nextLine());
        int paulScore = Integer.parseInt(scanner.nextLine());
        int opponentScore = Integer.parseInt(scanner.nextLine());

        if((paulScore + opponentScore)%(2*servesNumber)<servesNumber) {
            System.out.println("paul");
        }
        else{
            System.out.println("opponent");

        }
    }
}
