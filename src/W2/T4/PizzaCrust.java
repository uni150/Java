package W2.T4;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: K Pizza Crust
 * Link:https://open.kattis.com/contests/ggi5da/problems/pizza2
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 11/04/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.10 s
 */

import java.util.Scanner;
public class PizzaCrust {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double radius = scan.nextInt();
        double outermost = scan.nextInt();
        //calculate the percentage of the pizza that has cheese
        System.out.printf("%.6f" , 100 * ((radius-outermost)*(radius-outermost)) / (radius*radius));

        scan.close();
    }

}
