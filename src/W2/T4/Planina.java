package W2.T4;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem:E Planina
 * Link:https://open.kattis.com/contests/ggi5da/problems/planina
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 11/04/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.10 s
 */
import java.util.Scanner;

public class Planina {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberIterration = scanner.nextInt();
        int start = 2;
        double result = 0;
        //calculate the number of points stored after x  iterations
        for(int i = 1; i<= numberIterration; i++){
            start = start*2-1;
            result = Math.pow((double)start, 2);
        }
        System.out.println((int)result);
    }
}
