package W2.T4;
/**
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: J Quick Estimates
 * Link: https://open.kattis.com/contests/ggi5da/problems/quickestimate
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 11/04/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.11 s
 */

import java.util.Scanner;
public class QuickEstimate {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = scan.nextInt();

        while (number --> 0)
        {
            String estimatedCost = scan.next();
            System.out.println(estimatedCost.length());
        }

        scan.close();
    }
}
