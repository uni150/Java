package W2.T4;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: Q Server
 * Link:https://open.kattis.com/contests/ggi5da/problems/server
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/31/2022

 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.10 s
 */
import java.util.Scanner;

public class Server {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int tasksNumber = scanner.nextInt();
        int minutes = scanner.nextInt(); // the time we have to complete the tasks
        int[] tasksMinutes = new int[tasksNumber]; // initialize array length. The length should be equals to the numbers of the tasks.

        //input for all tasksMinutes
        for (int i = 0; i < tasksNumber; i++) {
            tasksMinutes[i] = scanner.nextInt();
        }

        int count = 0; // count the number of tasks that can be completed

        for (int i = 0; i < tasksNumber; i++) {
            minutes -=tasksMinutes[i]; // subtract the tasksMinutes

            if (minutes >= 0) // if we still have time left, add +1 to the count
                count++;
        }
        System.out.println(count);
        scanner.close();
    }
}
