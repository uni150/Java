package W2.T4;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: TakeTwoStones
 * Link: https://open.kattis.com/contests/ggi5da/problems/twostones
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 10/31/2022
 *
 * Method : Ad-Hoc
 * Status :Accepted
 * Runtime:0.10 s
 */
import java.util.Scanner;

public class TakeTwoStones {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberStones = Integer.parseInt(scanner.nextLine());

        if (numberStones % 2 == 1) {
            System.out.println("Alice");
        } else {
            System.out.println("Bob");
        }

    }
}
