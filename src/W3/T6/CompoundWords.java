package W3.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: H Compound Words
 * Link:https://open.kattis.com/contests/uk27ry/problems/compoundwords
 *
 * @Desislava Radusheva
 * @Gergana Georgieva
 * @version 1.0, 11/11/2022
 *
 * Method : Ad-Hoc
 * Status :Accepted
 * Runtime:0.16 s
 */

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
public class CompoundWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<String> words = new ArrayList<>();
        ArrayList<String> compound = new ArrayList<>();

        while (scanner.hasNext())
            words.add(scanner.next());

        for (int i = 0; i < words.size(); i++)
            for (int x = i + 1; x < words.size(); x++)
            {
                if (! compound.contains(words.get(i) + words.get(x)))
                    compound.add(words.get(i) + words.get(x));

                if (! compound.contains(words.get(x) + words.get(i)))
                    compound.add(words.get(x) + words.get(i));
            }

        Collections.sort(compound);

        for (int i = 0; i < compound.size(); i++)
            System.out.println(compound.get(i));

        scanner.close();
    }

}

