package W3.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: F Heliocentric
 * Link: https://open.kattis.com/contests/uk27ry/problems/heliocentric
 *
 * @author Desislava Radusheva
 * @author Gergana Georgieva
 * @version 1.0, 11/08/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.14 s.
 */

import java.util.Scanner;

public class Heliocentric {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int cases = 1; // case number starting at 1

        while (scanner.hasNextInt()) {
            System.out.print("Case " + cases + ": ");


            int earthDays = scanner.nextInt();
            int marsDays = scanner.nextInt();
            int days = 0;

            while (earthDays != 0 || marsDays != 0) {
                earthDays = (earthDays + 1) % 365;
                marsDays = (marsDays + 1) % 687;
                days++;
            }
            System.out.println(days);
            cases++;
        }
        scanner.close();
    }
}
