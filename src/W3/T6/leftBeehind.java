package W3.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: D Left Beehind
 * Link: https://open.kattis.com/contests/uk27ry/problems/leftbeehind
 * @author Desislava Radusheva
 * @author Gergana Georgieva
 * @version 1.0, 11/08/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.10 s.
 */

import java.util.Scanner;

public class leftBeehind {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        while (true) { // we use while, because without a label, break can only be used inside a loop or a switch.

            //input: quantity of the sweet/sour jar
            int sweetJar = scanner.nextInt();
            int sourJar = scanner.nextInt();

            if (sweetJar == 0 && sourJar == 0) {
                break; //Input is terminated by a line containing two zeros
            }
            if (sweetJar + sourJar == 13) {
                System.out.println("Never speak again.");
            } else if (sweetJar == sourJar) {
                System.out.println("Undecided.");
            } else if (sweetJar > sourJar) {
                System.out.println("To the convention.");
            } else {
                System.out.println("Left beehind.");
            }
        }
        scanner.close();
    }
}

