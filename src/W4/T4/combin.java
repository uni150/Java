package W4.T4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class combin {
    public static void main(String[] args) throws Exception
    {
        File file = new File(
                "E:\\JavaProject\\UniJava\\probe.txt");

        BufferedReader br
                = new BufferedReader(new FileReader(file));

        String sentence;
        while ((sentence = br.readLine()) != null)
            System.out.println(sentence);
         sentence = new String(Files.readAllBytes(Path.of("E:\\JavaProject\\UniJava\\probe.txt")));
                sentence = shuffle(sentence);
        System.out.println(sentence);
    }

    private static String shuffle(String sentence) {
        String[] words = sentence.split("\\s+");
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            List<Character> letters = new ArrayList<Character>();
            for (char letter : word.toCharArray()) {
                letters.add(letter);
            }
            if (letters.size() > 2) {
                Collections.shuffle(letters.subList(1, letters.size() - 2));
            }
            for (char letter : letters) {
                builder.append(letter);
            }
            builder.append(" ");
        }
        return builder.toString();
    }

}
