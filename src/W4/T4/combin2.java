package W4.T4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class combin2 {
    public static void main(String[] args) throws Exception {

        File file = new File(
                "E:\\JavaProject\\UniJava\\probe.txt");

        BufferedReader br
                = new BufferedReader(new FileReader(file));

        String sentence;
        while ((sentence = br.readLine()) != null)
            System.out.println(sentence);
        sentence = new String(Files.readAllBytes(Path.of("E:\\JavaProject\\UniJava\\probe.txt")));
        Random r = new Random();
        sentence = scramble(r, sentence);
        System.out.println(sentence);
    }

    public static String scramble( Random random, String inputString) {
        String sentence = "";
        String[] words = sentence.split("\\s+");
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            List<Character> letters = new ArrayList<Character>();
            for (char letter : word.toCharArray()) {
                letters.add(letter);
            }
            // Convert your string into a simple char array:
            //change
            char[] letter = inputString.toCharArray();

            // Scramble the letters using the standard Fisher-Yates shuffle,
            for (int i = 1; i < letter.length - 1; i++) {
                int j = random.nextInt(letter.length - 1);
                // Swap letters which are not the 1st and last one
                if (i != 0) {
                    char temp = letter[i];
                    letter[i] = letter[j];
                    letter[j] = temp;
                }
            }
            for (char ignored : letters) {
                builder.append(letter);
            }
            builder.append(" ");
        }
        return builder.toString();
        }
    }

