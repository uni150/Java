package W4.T4;

import java.io.*;

// Main class
// ReadingFromFile
public class dve {

    // Main driver method
    public static void main(String[] args) throws Exception
    {

        // Passing the path to the file as a parameter
        FileReader fr = new FileReader(
                "E:\\JavaProject\\UniJava\\probe.txt");

        // Declaring loop variable
        int i;
        // Holds true till there is nothing to read
        while ((i = fr.read()) != -1)

            // Print all the content of a file
            System.out.print((char)i);
    }
}