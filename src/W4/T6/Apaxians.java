package W4.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: J Apaxiaaaaaaaaaaaans!
 * Link: https://open.kattis.com/contests/tbnznz/problems/apaxiaaans
 *
 * @author Desislava Radusheva
 * @author Gergana Georgieva
 * @version 1.0, 11/18/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.14s.
 */
import java.util.Scanner;

public class Apaxians {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String name = scanner.nextLine();
        String ret = "";

        for (int i = 0; i < name.length(); i++)
            if ((i == name.length() - 1) || name.charAt(i) != name.charAt(i + 1))
                ret += name.charAt(i);

        System.out.println(ret);

        scanner.close();
    }
}
