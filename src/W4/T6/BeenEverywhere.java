package W4.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: G I've Been Everywhere, Man
 * Link: https://open.kattis.com/contests/tbnznz/problems/everywhere
 *
 * @author Desislava Radusheva
 * @author Gergana Georgieva
 * @version 1.0, 11/15/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.15 s.
 */

import java.util.HashSet;
import java.util.Scanner;

public class BeenEverywhere {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int testCases = Integer.parseInt(scanner.nextLine());

        for(int n = 0; n<testCases; n++ ) {
            HashSet<String> set = new HashSet<>();

            int cities = scanner.nextInt();

            for (int i = 0; i < cities; i++)
                set.add(scanner.next());

            System.out.println(set.size());
        }
        scanner.close();
        }
    }

