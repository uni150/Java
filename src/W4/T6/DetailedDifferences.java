package W4.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: K Detailed Differences
 * Link: https://open.kattis.com/contests/tbnznz/problems/detaileddifferences
 *
 * @author Desislava Radusheva
 * @author Gergana Georgieva
 * @version 1.0, 11/18/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.22s.
 */

import java.util.Scanner;

public class DetailedDifferences {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int testCases = scanner.nextInt(); // input for the number of test cases

        for (int i = 0; i < testCases; i++) { // input for the actual cases
            String str1 = scanner.next();
            String str2 = scanner.next();

            System.out.println(str1); // print the cases
            System.out.println(str2);

            for (int y = 0; y < str1.length(); y++)
                //comparing the strings
                //charAt - Returns the char value at the specified index
                if (str1.charAt(y) == str2.charAt(y))
                    System.out.print(".");
                else
                    System.out.print("*");

            System.out.println();
        }
        scanner.close();
    }
}
