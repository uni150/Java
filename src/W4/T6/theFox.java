package W4.T6;
/*
 * Advanced Object Oriented Programming with Java, WS 2022
 * Problem: T What does the fox say?
 * Link: https://open.kattis.com/contests/tbnznz/problems/whatdoesthefoxsay
 *
 * @author Desislava Radusheva
 * @author Gergana Georgieva
 * @version 1.0, 11/18/2022
 *
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.15s.
 */
import java.util.ArrayList;
import java.util.Scanner;

public class theFox {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int testCases = scanner.nextInt();
        scanner.nextLine();
        for (int x=0;x<testCases;x++){
            String[] sounds = scanner.nextLine().split(" ");
            ArrayList<String> notfox = new ArrayList<>();

            while(true) {
                String str = scanner.nextLine();
                if(str.equals("what does the fox say?"))
                    break;
                notfox.add(str.split(" ")[2]);
            }
            for(int y=0;y< sounds.length;y++)
                if(!notfox.contains(sounds[y]))
                    System.out.print(sounds[y]+ " ");
        }
        scanner.close();
    }
}
